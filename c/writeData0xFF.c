// source : http://www.unixgarden.com/index.php/gnu-linux-magazine-hs/programmation-du-port-parallele
// write by Pilatomic
// 
#include <sys/ioctl.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/errno.h>
#include <linux/ppdev.h>
#include <linux/parport.h>

void main(){
        int errno, fd;
	unsigned char valout=0xff;
        printf("Test port parallèle\n");
        
        //opening port
        if ((fd = open("/dev/parport0", O_RDWR)) < 0) {
                fprintf(stderr,"Open Error : %s (%d)\n",
                strerror(errno),errno);
                exit(EXIT_FAILURE);
        }
        
        //locking port
        if (ioctl(fd, PPCLAIM) < 0) {
  		fprintf(stderr,"PPCLAIM ioctl Error : %s (%d)\n",
    		strerror(errno),errno);
  		exit(EXIT_FAILURE);
	}
	
	//write the value VALOUT to the port
	 
	if (ioctl (fd, PPWDATA, &valout) < 0) {
	  fprintf(stderr,"PPWDATA ioctl Error : %s (%d)\n",
	      strerror(errno),errno);
	  exit(EXIT_FAILURE);
	}	
	
	//releasing port
	if (ioctl(fd, PPRELEASE) < 0) {
  		fprintf(stderr,"PPRELEASE ioctl Error : %s (%d)\n",strerror(errno),errno);
  		exit(EXIT_FAILURE);
	}
 	
 	//closing port
	if(close(fd) < 0) {
  		fprintf(stderr,"Close Error : %s (%d)\n",
      		strerror(errno),errno);
  		exit(EXIT_FAILURE);
	}	
}       

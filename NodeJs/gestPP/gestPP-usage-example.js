#!/usr/bin/env node
/*
nom: gestPP-0.2-test.js
//https://npmjs.org/package/parport
//https://code.google.com/p/parallel-port/
//http://www.unixgarden.com/index.php/gnu-linux-magazine-hs/programmation-du-port-parallele

//install le module parport
//   #npm install parport
*/

console.log('**************** gestPP-0.2-test ********************************');

//
function sleep(milliseconds)
	{
	var start=new Date().getTime();
	for (var i=0;i<1e7;i++){if(new Date().getTime()-start>milliseconds)break;}
	}



erreur=null;
// chargement de la lib gestPP
this.erreur=null;
var libPath='/www/sites/intersites/lib/perso/nodejs/gestPP/gestPP-0.2.js';
console.log('Librairie "'+libPath+'": chargement');
try{gestPP=require(libPath).gestPP;}
catch(err){this.erreur=err;}
finally{if(this.erreur===null){console.log('Librairie "'+libPath+' chargé."');}else{console.log('Librairie "'+libPath+'" !!!! ERREUR !!! : '+this.erreur);}}
console.log('erreur:'+gestPP.erreur);

gestPP.test();
//gestPP.nettoyage();
//gestPP.test_modeZone('data');
//gestPP.test_modeZone('control');
//gestPP.test_modePin();

console.log('************************************************');

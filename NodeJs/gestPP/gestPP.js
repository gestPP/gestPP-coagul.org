/*******************************
gestion du Port Parallele
version:0.2
Utilise le module parport
*********************************/
function gestPP(conf)
	{
	this.erreur=null;
	this.modPar=null;
	this.port=null;
	this.dataNow=null;//valeur actuelle du PP
	//configuration
	this.conf=conf?conf:{};
	this.conf.parportPath=this.conf.parportPath?this.conf.parportPath:'/lib/node_modules/parport';

	// ==== chargement de la lib gestBits =====
	this.erreur=null;
	var libPath='/www/sites/intersites/lib/perso/js/gestBits/gestBits-0.1-node.js';
	console.log('Librairie "'+libPath+'": chargement');
	try{this.gestBits=require(libPath).gestBits;}
	catch(err){this.erreur=err;}
	finally{if(this.erreur===null){console.log('Librairie "'+libPath+'" chargé.');}else{console.log('Librairie "'+libPath+' !!!! ERREUR !!! : '+this.erreur);}}

	// ==== chargement du module parport  ====
	this.erreur=null;
	var libPath=this.conf.parportPath;
	console.log('Librairie "'+libPath+'": chargement');
	try{this.modPar=require(libPath);}
	catch(err){this.erreur=err;}
	finally{if(this.erreur===null){console.log('Librairie "'+libPath+'" chargé.');}else{console.log('Librairie "'+libPath+' !!!! ERREUR !!! : '+this.erreur);}}

	// ==== connexion au port parallele  ====
	if(this.modPar)
		{
		console.log('connexion au port parallele: ');
		this.erreur=null;
		var consoleOut='';
		try{this.port=new this.modPar.Port();}
		catch(err){this.erreur=err;}
		finally{if(this.erreur===null){consoleOut+='connexion au port parallele:connecté.';}else{consoleOut+=' !!!! ERREUR !!! : '+this.erreur;}console.log(consoleOut);}
		}

	this.sleep=function(milliseconds)
		{
		var start=new Date().getTime();
		for (var i=0;i<1e7;i++){if(new Date().getTime()-start>milliseconds)break;}
		}


	/****************************
	fonctions RW physique au port
	*****************************/

	/* ===== Ligne Data ===== */ 

	// renvoie l'etat binaire du bit specifié OU l'etat en decimal du Port Par
	this.readData=function(bit)
		{
		if(this.port==null)return null;
		this.dataNow=this.port.readData();
		var out=this.dataNow;if(bit>=0 && bit<=7){out=this.gestBits.showBit(this.dataNow,bit);}return out;
		}

	this.writeData=function(){if(this.port==null)return null;this.port.writeData();}

	/* ===== Ligne Status ===== */ 
	this.readStatus=	function(bit)
		{
		if(this.port==null)return;
		statusNow=this.port.readStatus();
		var out=statusNow;
		if(bit>=0 && bit<=3) out=this.gestBits.showBit(this.statusNow,bit);
		return out;
		}

	/* ===== Ligne Control ===== */ 
	this.readControl=	function(bit)
		{
		if(this.port!=null)return;
		var controlNow=this.port.readControl();
		var out=controlNow;
		if(bit>=0 && bit<=this.controlBitMax) out=this.gestBits.showBit(this.controlNom,bit);
		return out;
		}

	/****************************
	mode par  zone
	*****************************/
	this.bitNb=[];
	this.bitNb['data']=8;
	this.bitNb['status']=3;
	this.bitNb['control']=5;

	this.data=[];
	this.data['data']=0;
	this.data['status']=0;
	this.data['control']=0;


	this.isZone=function(zone){if(zone=='data'||zone=='control'||zone=='status')return 1;return 0;}

	// RW physique integer
	this.readZone=function(zone)
		{
		if(this.port==null)return null;
		var r=null;
		switch(zone)
			{
			case'data':	r=this.port.readData();break;
			case'status':	r=this.port.readStatus();break;
			case'control':	r=this.port.readControl();break;
			}
		return r;
		}

	this.writeZone=function(zone,data)
		{
		if(this.port==null)return null;
		switch(zone)
			{
			case'data':	this.port.writeData(data?data:this.data[zone]);break;
			case'control':	this.port.writeControl(data?data:this.data[zone]);break;
			}
		}

	/****************************
	// Gestion bit a bit
	// ATTENTION ne gere pas les portes inverses
	une mise a 1 provoque l'activation du pin meme sur une porte inverse(qui est mise a 0)
	********************************/

	this.setAll=function(zone){this.writeZone(zone,255);}
	this.clrAll=function(zone){this.writeZone(zone,0);}


	this.setBit=function(zone,bit)
		{
		if(this.isZone(zone))
			{
			if(bit>=0 && bit<this.bitNb[zone])
				{
				this.writeZone(zone,this.gestBits.setBit(this.readZone(zone),bit));
				}
			}
		}
	
	this.clrBit=function(zone,bit)
		{
		if(this.isZone(zone))
			{
			if(bit>=0 && bit<this.bitNb[zone])
				{
				this.writeZone(zone,this.gestBits.clrBit(this.readZone(zone),bit));
				}
			}
		}

	this.notBit=function(zone,bit)
		{

		if(this.isZone(zone))
			{
			if(bit>=0 && bit<this.bitNb[zone])
				{
				this.writeZone(zone,this.gestBits.notBit(this.readZone(zone),bit));
				}
			}
		}


	/****************************
	mode par  zone
	attention: gere les portes inverse
	une mise a 1 provoque l'activation du pin meme sur une porte inverse
	*****************************/
	// mode par PIN
	this.pin=[];
	//data
	this.pin['D0']=2;
	this.pin['D1']=3;
	this.pin['D2']=4;
	this.pin['D3']=5;
	this.pin['D4']=6;
	this.pin['D5']=7;
	this.pin['D6']=8;
	this.pin['D7']=9;
	//control
	this.pin['STROBE']=1;
	this.pin['LINEFEED']=14;
	this.pin['INITIALIZE']=16;
	this.pin['SELECTPRINTER']=17;

	//status
	this.pin['ACK']=10;
	this.pin['BUSY']=11;
	this.pin['PAPEROUT']=12;
	this.pin['SELECT']=13;

	//
	this.setPin=function(pin)
		{
		switch(pin)
			{
			//data
			case 2: this.setBit('data',0);break;
			case 3: this.setBit('data',1);break;
			case 4: this.setBit('data',2);break;
			case 5: this.setBit('data',3);break;
			case 6: this.setBit('data',4);break;
			case 7: this.setBit('data',5);break;
			case 8: this.setBit('data',6);break;
			case 9: this.setBit('data',7);break;

			//control(RW)
			case 1: this.clrBit('control',0);break;//inv
			case 14: this.clrBit('control',1);break;//inv
			case 16: this.setBit('control',2);break;
			case 17: this.clrBit('control',3);break;//inv
				
			//status(R)
//			case 10: this.setBit('status',6);break;
//			case 11: this.setBit('status',7);break;//inv
//			case 12: this.setBit('status',5);break;
//			case 13: this.setBit('status',4);break;
//			case 15: this.setBit('status',3);break;
			}
		}
	
	this.clrPin=function(pin)
		{
		switch(pin)
			{
			//data
			case 2: this.clrBit('data',0);break;
			case 3: this.clrBit('data',1);break;
			case 4: this.clrBit('data',2);break;
			case 5: this.clrBit('data',3);break;
			case 6: this.clrBit('data',4);break;
			case 7: this.clrBit('data',5);break;
			case 8: this.clrBit('data',6);break;
			case 9: this.clrBit('data',7);break;

			//control(RW)
			case 1: this.setBit('control',0);break;//inv
			case 14: this.setBit('control',1);break;//inv
			case 16: this.clrBit('control',2);break;
			case 17: this.setBit('control',3);break;//inv
			}
		}

	this.notPin=function(pin)
		{
		switch(pin)
			{
			//data
			case 2: this.notBit('data',0);break;
			case 3: this.notBit('data',1);break;
			case 4: this.notBit('data',2);break;
			case 5: this.notBit('data',3);break;
			case 6: this.notBit('data',4);break;
			case 7: this.notBit('data',5);break;
			case 8: this.notBit('data',6);break;
			case 9: this.notBit('data',7);break;

			//control(RW)
			case 1: this.notBit('control',0);break;
			case 14: this.notBit('control',1);break;
			case 16: this.notBit('control',2);break;//inv
			case 17: this.notBit('control',3);break;
			}
		}


	/******************************
	NETTOYAGE
	******************************/

	this.nettoyage=function()
	{
		console.log('Nettoyage');

		//   1 -> 0 -> 1
		console.log('setAll():');this.setAll('data');
		console.log('setAll():');this.setAll('control');
		this.sleep(2000);

		console.log('clrAll():');this.clrAll('data');
		console.log('clrAll():');this.clrAll('control');
		this.sleep(250);

		console.log('setAll():');this.setAll('data');
		console.log('setAll():');this.setAll('control');
		this.sleep(500);

		console.log('clrAll():');this.clrAll('data');
		console.log('clrAll():');this.clrAll('control');
		this.sleep(500);
		return;
		}


	/****************************
	function de test
	*****************************/
	this.test=function()
		{
		this.nettoyage();
		this.test_modeZone('data');
		this.test_modeZone('control');
		this.test_modePin();
		}
	/******************************
	VERIFICATION DU MODE BIT A BIT ZONE
	******************************/

	this.test_modeZone=function(zone,delay)
		{
		var delay=delay?delay:1000;
		if (!(zone=='data'||zone=='control')){return;}
	
		console.log('zone:'+zone);
		console.log('****************  VERIFICATION DU MODE ZONE '+zone+' *******************');

		// 0
		console.log('setAll():');this.setAll('data');this.sleep(delay);
		console.log('clrAll():');this.clrAll('data');this.sleep(delay);

		// 1 par succession croissant
		console.log('1 par succession croissant:');
		for (var bit=0;bit<this.bitNb[zone];bit++){this.setBit(zone,bit);this.sleep(delay);}

		// 0 par succession decroissant
		console.log('0 par succession decroissant:');
		for (var bit=this.bitNb[zone]-1;bit>=0;bit--){this.clrBit(zone,bit);this.sleep(delay);}

		// lecture decimal
		console.log('this.readZone('+zone+'):'+this.readZone(zone));
		}

	/******************************
	VERIFICATION DU MODE PIN
	******************************/
	this.test_modePin=function()
		{
		console.log('***************VERIFICATION DU MODE PIN  *******************');

		console.log('clrAll():');this.clrAll('data');
		console.log('clrAll():');this.clrAll('control');

		//set
		for (var pin=1;pin<=9;pin++){console.log('setPin('+pin+'):');this.setPin(pin);this.sleep(300);}
		pin=14;console.log('setPin('+pin+'):');this.setPin(pin);this.sleep(300);
		pin=16;console.log('setPin('+pin+'):');this.setPin(pin);this.sleep(300);
		pin=17;console.log('setPin('+pin+'):');this.setPin(pin);this.sleep(300);

		//clr
		pin=17;console.log('clrPin('+pin+'):');this.clrPin(pin);this.sleep(300);
		pin=16;console.log('clrPin('+pin+'):');this.clrPin(pin);this.sleep(300);
		pin=14;console.log('clrPin('+pin+'):');this.clrPin(pin);this.sleep(300);
		for (var pin=9;pin>=0;pin--){console.log('clrPin('+pin+'):');this.clrPin(pin);this.sleep(300);}
		}

	}	//gestPP()

//activer en tant que module node.js
exports.gestPP=new gestPP();

// Binary Counter
// by Dlareg - 23/08/2013
// This program counts 0 to 255

function timer(ms) {
	ms += new Date().getTime();
	while (new Date() < ms){}
}

function init() {
	port.writeData(0);
}

var par = require('parport');
var port = new par.Port();

console.log('Welcom in binaryCounter !');
// change the value to slow down or speed up the display
tempo=25;
// reset value
init();
timer(2000);

for (i=0; i<=255; i++) {
	port.writeData(i);
	console.log(i);
	timer(tempo);
}
init();
console.log('binaryCounter finished counting');
